import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:ju/Player/Player.dart';
import 'package:ju/api/juAPi.dart';

class LiveListPage extends StatefulWidget{
  String name;
  LiveListPage({this.name});
  @override
  State<StatefulWidget> createState() {
    return new LiveListPageWidgetState();
  }
}
class LiveListPageWidgetState extends State<LiveListPage>{

  List list=[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getList();

  }

  Future getList() async {
    juApi api=new juApi();
    Response response=await api.LiveList(this.widget.name);
    List lists = response.data['data']['lists'];
    print(lists);
    setState(() {
      list = lists;
    });
  }
  void toPlayer(item){
    Navigator.of(context).push(new MaterialPageRoute(builder: (_){
      return new PlayerPage(item:item);
    }));
  }
  Widget buildList() {
    List<Widget> rows = [];//先建一个数组用于存放循环生成的widget
    print(list.length);
    if(list.length != null){
      for(var item in list) {
        rows.add(
            new FlatButton(
              onPressed: () {
                this.toPlayer(item);
              },
              child: Column(
                children: <Widget>[
                  Image(
                    image: NetworkImage(item["img"]),
                    width: 120,
                  ),
                  Text(item["title"])
                ],
              ),
            )
        );
      }
    }
    return GridView(
        scrollDirection: Axis.vertical,
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 100, //子控件最大宽度为100
          childAspectRatio: 0.5,//宽高比为1:2
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
        ),
        padding: EdgeInsets.all(10),
        children: rows
    );
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(this.widget.name),
        ),
        body:Container(
            child: buildList()
        )
    );
  }

}