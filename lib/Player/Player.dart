import 'package:fijkplayer/fijkplayer.dart';
import 'package:flutter/material.dart';

class PlayerPage extends StatefulWidget{
  Map item;
  PlayerPage({this.item});
  @override
  State<StatefulWidget> createState() {
    return new PlayerPageWidgetState();
  }
}
class PlayerPageWidgetState extends State<PlayerPage>{

  FijkPlayer _controller;
  FijkView playerWidget;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(this.widget.item["play_url"]);
    _controller = new FijkPlayer();
    _controller.setDataSource(this.widget.item["play_url"], autoPlay: true);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('正在播放：'+this.widget.item["title"]),
        ),
        body: Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                Expanded(
                    child: FijkView(
                      width: 300,
                      height: 300,
                      player: _controller,
                    )
                ),
              ],
            ),
            new Container(
              color: Colors.blue,
              width: 100.0,
              height: 100.0,
            ),
          ],
        ),
    );
  }
  @override
  void dispose() {
    _controller.release();
    super.dispose();
  }

}