import 'dart:io';
import 'package:dio/dio.dart';

class juApi {
  String host="http://tuzi888.top";
  String Token ="";
  BaseOptions _options=new BaseOptions(contentType:ContentType.parse("application/x-www-form-urlencoded"));
  Dio dio=null;
  juApi(){
    this.dio=new Dio(_options);
    this.dio.interceptors.add(InterceptorsWrapper(
        onRequest:(Options options) async{
          options.headers["token"] = "4af0b1e14b5483d6b529cab6f546956f";
          return options; //continue
        }
    ));
  }
//  获取token
  Future Login () async {
    Response response=await this.dio.post(this.host+"/mobile/user/login",data: {}, options: new Options(contentType:ContentType.parse("application/x-www-form-urlencoded")));
    this.Token=response.data["data"]["token"];
  }
//  直播首页
  Future<Response> LiveCatList() async {
    try {
      Response response=await this.dio.post(this.host+"/mobile/live/index");
      return response;
    } catch (e) {
      print(e);
    }
  }
  Future<Response> LiveList(String name) async{
    Response response=await this.dio.post(this.host+"/mobile/live/anchors",data: {"name":name});
    return response;
  }
}