import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:ju/api/juAPi.dart';
import 'package:ju/liveList/liveList.dart';


class IndexPageWidget extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new IndexPageWidgetState();
  }
}

class IndexPageWidgetState extends State<IndexPageWidget>{
  List list=[];
  Future getList() async {
    juApi api=new juApi();
    Response response=await api.LiveCatList();
    print(1);
    print(response);
    List user = response.data['data']['lists'];
    setState(() {
      list = user;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getList();

  }
  void toList(row){
    print(row['name']);
    Navigator.of(context).push(new MaterialPageRoute(builder: (_){
      return new LiveListPage(name : row['name']);
    }));
  }

  Widget buildList() {
    List<Widget> rows = [];//先建一个数组用于存放循环生成的widget
    print(list.length);
    if(list.length != null){
      for(var item in list) {
        rows.add(
            new FlatButton(
              onPressed: () {
                this.toList(item);
              },
              child: Column(
                children: <Widget>[
                  Image(
                    image: NetworkImage(item["img"]),
                    width: 120,
                  ),
                  Text(item["title"])
                ],
              ),
            )
        );
      }
    }
    return GridView(
      scrollDirection: Axis.vertical,
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 100, //子控件最大宽度为100
        childAspectRatio: 0.5,//宽高比为1:2
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
      padding: EdgeInsets.all(10),
      children: rows
    );
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('首页'),
      ),
      body:Container(
        width: double.infinity,
        child: buildList()
      )
    );
  }
}

