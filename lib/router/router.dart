import 'package:flutter/material.dart';
import 'package:ju/Index/Index.dart';
import 'package:ju/Video/video.dart';
import 'package:ju/My/my.dart';

class Router {
  Map<String, WidgetBuilder> List(){
    return <String, WidgetBuilder> {
      "/index":(BuildContext context) => new IndexPageWidget(),
      "/video":(BuildContext context)=> new  VideoPageWidget(),
      "/my":(BuildContext context)=>new MyPageWidget()
    };
  }
}